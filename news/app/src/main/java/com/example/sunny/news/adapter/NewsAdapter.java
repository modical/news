package com.example.sunny.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sunny.news.R;
import com.example.sunny.news.struct.News;
import com.example.sunny.news.utils.HttpUtils;

import java.util.List;

/**
 * Created by Sunny on 2015/12/6.
 */
public class NewsAdapter extends BaseAdapter {

    private Context context;
    private List<News> newsList;

    public NewsAdapter(Context context,List<News> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @Override
    public int getCount() {
        return newsList.size();
    }

    @Override
    public News getItem(int position) {
        return newsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.news_item,null);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        TextView tvDesc = (TextView) convertView.findViewById(R.id.tvDesc);
        TextView tvTime = (TextView) convertView.findViewById(R.id.tvTime);
        ImageView ivPic = (ImageView) convertView.findViewById(R.id.ivNew);
        tvTitle.setText(newsList.get(position).title);
        tvDesc.setText(newsList.get(position).desc);
        tvTime.setText(newsList.get(position).time);
        ivPic.setImageBitmap(HttpUtils.getImageFromUrl(newsList.get(position).pic_url));

        return convertView;
    }
}

