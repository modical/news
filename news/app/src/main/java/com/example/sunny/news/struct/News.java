package com.example.sunny.news.struct;

/**
 * Created by Sunny on 2015/12/6.
 */
public class News {
   public String title;
   public String desc;
   public String time;
   public String content_url;
   public String pic_url;
}
